
Feature: Product Listing
  
  Scenario: List Apple iPhone 8 products having red color and verify if it’s red color after randomly selecting one of them displayed on the list
    Given access to www.ebay.com
    And click electronics tab
    And click phones,smart watches and accessories on shop by category menu
    And click phones,smartphone parts on shop by category menu
    And click cell phones and smartphones on shop by category menu
    And select apple as brand
    And select iphone eight as product
    And select red as product color
    Then take all red iphone eight red products and verify if it’s red color after randomly selecting one of them displayed on the list


