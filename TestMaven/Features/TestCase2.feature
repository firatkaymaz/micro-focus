
Feature: Checking the functionality of saved search

  Scenario: verify specific product inquired through search box is listed 
and save concerned search and verify it is listed in saved searches area 
then delete that product and verify it’s been deleted

    Given access to ebay.com
    And search specific product
    When save the concerned search
    And login to ebay
    And follow the path myebay and saved searches
    Then verify the product being searched is in the list
    Then verify the product being searched has been deleted from the list

