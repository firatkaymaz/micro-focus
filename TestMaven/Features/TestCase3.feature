
Feature: Testing of watchlist functionality

  Scenario: pick a blue, xl and lower than fifty dollar men tshirt by adding and verifying it in the watch list
  
    Given open www.ebay.com
    And click shop by category
    And click men under fashion
    And click mens shirt under mens clothing
    And click xlarge
    And select blue color
    And type fifty dollar into max field
    And select a random blue tshirt
    And add the product into watch list
    And type your username and password
    And follow the watchlist path
    Then verify the product is in the watchlist

