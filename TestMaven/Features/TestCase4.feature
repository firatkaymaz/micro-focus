
Feature: Verification of the product price

  Scenario: verify the product price once the quantity amount is doubled
    Given open ebay website
    And search the product in search box and press the button
    And click the concerned product
    And select storage capacity
    And click add to cart button
    Then make quantity two and verify the product's price has been doubled
   


