
Feature: Testing of recently viewed's functionality

  Scenario: Checking if the product viewed is in the recently viewed screen
    Given visit to ebay website
    And click motors tab
    And click cars and trucks on the left side menu
    And select a random brand
    And click the item
    When go to recently viewed through my ebay
    Then verify the item viewed in latest is in the recently viewed screen
  

