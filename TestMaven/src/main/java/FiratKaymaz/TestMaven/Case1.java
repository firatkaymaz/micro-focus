package FiratKaymaz.TestMaven;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Case1 {
	
	public static WebDriver driver;
	
	@Given("^access to www.ebay.com$")	
	public void AccesstoEbay() {
	//System.setProperty("webdriver.gecko.driver","/Users/firatkaymaz/eclipse-workspace/SeleniumTest/drivers/geckodriver/geckodriver");
	WebDriverManager.firefoxdriver().setup();
	driver = new FirefoxDriver();
	driver.get("https://www.ebay.com/");		
	}
	@And("^click electronics tab$")
	public void Electronics() {
		driver.findElement(By.xpath("/html/body/div[4]/div[1]/ul/li[3]/a")).click();
	}
	@And("^click phones,smart watches and accessories on shop by category menu$")
	public void Title1() {
	driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[1]/div/div/div/section/ul/li[1]/a")).click();	
	}
	@And("^click phones,smartphone parts on shop by category menu$")
	public void Title2() {
	driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[1]/div/div/div/section/ul/li[1]/a")).click();	
	}
	@And("^click cell phones and smartphones on shop by category menu$")
	public void Title3() {
		driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[1]/div/div/div/section/ul/li[3]/a")).click();
		}
	@And("^select apple as brand$")
	public void Filter1() {
		driver.findElement(By.xpath("//li[@name='Brand']//input[@aria-label='Apple']")).click();
		}
	@And("^select iphone eight as product$")
	public void Filter2() {
		driver.findElement(By.xpath("//li[@name='Model']//input[@aria-label='Apple iPhone 8']")).click();
		}
	@And("^select red as product color$")
	public void Filter3() {
		driver.findElement(By.xpath("//li[@name='Color']//input[@aria-label='Red']")).click();
		}
	@Then("^take all red iphone eight red products and verify if it’s red color after randomly selecting one of them displayed on the list$")
	public void Verify() {
		List<WebElement> elementname = driver.findElements(By.xpath("//li[contains(@id,'w6-')]"));
		System.out.println("Number of total Red Iphone 8 product having red colour on the screen" + " " + "is" + " " +  elementname.size());
		Random rand = new Random();
		int value = rand.nextInt(elementname.size());
		if(elementname.get(value).findElement(By.xpath("//span[contains(@class,'Attributes3')]")).getText().contains("Red")) {
			if(value==0) {
			System.out.println(value+1 +"." + "product's colour is" + " " + elementname.get(value).findElement(By.xpath("//span[contains(@class,'Attributes3')]")).getText());
		}else {	
				System.out.println(value +"." + "product's colour is" + " " + elementname.get(value).findElement(By.xpath("//span[contains(@class,'Attributes3')]")).getText());}}
		}
}
