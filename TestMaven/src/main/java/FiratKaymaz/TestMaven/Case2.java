package FiratKaymaz.TestMaven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Case2 {

	public static WebDriver driver;
	
	@Given("^access to ebay.com$")	
	public void accesstoEbay() {
		WebDriverManager.firefoxdriver().setup();
		//System.setProperty("webdriver.gecko.driver","/Users/firatkaymaz/eclipse-workspace/SeleniumTest/drivers/geckodriver/geckodriver");
		driver = new FirefoxDriver();
		driver.get("https://www.ebay.com/");		
	}
	@And("^search specific product$")
	public void searchSpecificProduct() {
		driver.findElement(By.xpath("//*[@id=\"gh-ac\"]")).sendKeys("Apple iMac Pro 27\" with Retina 5K Display (1TB SSD, Intel Xeon, 3.20GHz, 32GB)");
		driver.findElement(By.xpath("//*[@id=\"gh-btn\"]")).click();
	}
	@When("^save the concerned search$")
	public void clicktheProduct() {
	driver.findElement(By.xpath("//*[@id=\"w4-w5-follow-follow-faux-btn\"]")).click();
	}
	@And("^login to ebay$")
	public void login() {
		driver.findElement(By.xpath("//*[@id=\"userid\"]")).sendKeys("firat_14");
		driver.findElement(By.xpath("//*[@id=\"pass\"]")).sendKeys("1a2b3c4d");
		driver.findElement(By.xpath("//*[@id=\"sgnBt\"]")).click();
	}
	@And("^follow the path myebay and saved searches$")
	public void followthepath() throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='gh-menu']//a[@class='gh-eb-li-a']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//p[@class='menu-head  ']//a[@data-pagetitle='Saved searches']")).click();
		Thread.sleep(3000);	
	}
	@Then("^verify the product being searched is in the list$")
	public void verifythesearchedproduct(){
		
		WebElement image = driver.findElement(By.xpath("//img[contains(@alt,'apple imac pro 27')]"));
		if(image.isDisplayed()) {
			System.out.println("Product has just been added into saved searches");
		}else {
			System.out.println("Product couldn't be added into saved searches");
		}
	}
	@Then("^verify the product being searched has been deleted from the list$")
	public void deleteproduct() throws InterruptedException {
		WebDriverWait wait =  new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/div[2]/div[3]/div[3]/div[3]/table/tbody/tr/td[1]/div/div[2]/div[2]/div[1]/a/span")));
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[3]/div[3]/div[3]/table/tbody/tr/td[1]/div/div[2]/div[2]/div[1]/a/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[3]/div[3]/div[3]/div[1]/button[1]")).click();
		System.out.println("Product was deleted");
     	try {
     		driver.findElement(By.xpath("//div[@id='16211600206')]"));
     	} catch (org.openqa.selenium.NoSuchElementException e) {
     		System.out.println("Deleted product is not listed.It's verified");
     	}
		
	}
	
}
