package FiratKaymaz.TestMaven;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Case3 {
	
public static WebDriver driver;
		
	@Given("^open www.ebay.com$")	
	public void AccesstoEbay() {
		WebDriverManager.firefoxdriver().setup();
		//System.setProperty("webdriver.gecko.driver","/Users/firatkaymaz/eclipse-workspace/SeleniumTest/drivers/geckodriver/geckodriver");
		driver = new FirefoxDriver();
		driver.get("https://www.ebay.com/");		
	}
	@And("^click shop by category$")
	public void shopbycategory() {
		driver.findElement(By.xpath("//*[@id=\"gh-shop-a\"]")).click();
	}
	@And("^click men under fashion$")
	public void fashionmen() {
		driver.findElement(By.xpath("/html/body/header/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[1]/ul[3]/li[2]/a")).click();
	}
	@And("^click mens shirt under mens clothing$")
	public void mensshirt() {
		driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[1]/div/div/div/section/ul/li[6]/a")).click();
	}
	@And("^click xlarge$")
	public void xlarge() {
		driver.findElement(By.xpath("//li[@name='Size%2520%2528Men%2527s%2529']//div[@id='w1-w1-w0-w0-multiselect[4]']//input[@aria-label='XL']")).click();
	}
	@And("^select blue color$")
	public void blue() {
		driver.findElement(By.xpath("//ul[@class='x-color-picker__body']//a[@title='Blue']")).click();
	}
	@And("^type fifty dollar into max field$")
	public void pricerange() {
		driver.findElement(By.xpath("//input[@aria-label='Maximum Value']")).sendKeys("50");
		driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[1]/div/div/div/ul/div/li[2]/ul/li/div[2]/div/button")).click();
	}
	@And("^select a random blue tshirt$")
	public void pickathshirt() {
		Random rand = new Random();
		int value = rand.nextInt(20) + 1;
		try {
			driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[3]/section[1]/ul/li["+value+"]/div/div[1]/div/a/div/img")).click();
     	} catch (org.openqa.selenium.NoSuchElementException e) {
     		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[3]/section[2]/ul/li["+value+"]/div/div[1]/div/a/div/img")).click();
     	}
	}
	@And("^add the product into watch list$")
	public void watchlist() {
		try {
			driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[2]/div[3]/div[2]/form/div[2]/div[2]/div[3]/div[1]/a")).click();
     	} catch (org.openqa.selenium.NoSuchElementException e) {
     		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div/div/div[3]/div[3]/div[2]/form/div[2]/div[2]/div[3]/div[1]/a")).click();
     	}
	}
	@And("^type your username and password$")
	public void loginto() {
		driver.findElement(By.xpath("//*[@id=\"userid\"]")).sendKeys("firat_14");
		driver.findElement(By.xpath("//*[@id=\"pass\"]")).sendKeys("1a2b3c4d");
		driver.findElement(By.xpath("//*[@id=\"sgnBt\"]")).click();	
	}
	@And("^follow the watchlist path$")
	public void watchlistpath() {
		WebDriverWait wait =  new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//html/body/div[3]/div[2]/div/div/header/div/ul[2]/li[1]/div/a[1]")));
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//html/body	/div[3]/div[2]/div/div/header/div/ul[2]/li[1]/div/a[1]"))).perform();
		WebDriverWait wait2 =  new WebDriverWait(driver,15);
		wait2.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[3]/div[2]/div/div/header/div/ul[2]/li[1]/div/div/ul/li[4]/a")));
		Actions act2 = new Actions(driver);
		act2.moveToElement(driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/header/div/ul[2]/li[1]/div/div/ul/li[4]/a"))).click().perform();
	}
	@Then("^verify the product is in the watchlist")
	public void watchlistverification() throws InterruptedException {
		Thread.sleep(3000);
		if(driver.findElement(By.xpath("//div[@class='item-details item-clmn']")).isDisplayed()) {
			System.out.println("Product is in the watch list");
		}
	}
}
