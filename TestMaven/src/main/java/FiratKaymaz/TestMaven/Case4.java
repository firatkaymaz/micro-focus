package FiratKaymaz.TestMaven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Case4 {
	
	public static WebDriver driver;
	
	@Given("^open ebay website$")	
	public void accesstoEbay() {
		WebDriverManager.firefoxdriver().setup();
		//System.setProperty("webdriver.gecko.driver","/Users/firatkaymaz/eclipse-workspace/SeleniumTest/drivers/geckodriver/geckodriver");
		driver = new FirefoxDriver();
		driver.get("https://www.ebay.com/");		
	}
	@And("^search the product in search box and press the button$")
	public void searchSpecificProduct() {
		driver.findElement(By.xpath("//*[@id=\"gh-ac\"]")).sendKeys("SanDisk 8GB 16GB 32GB 64GB Cruzer Blade USB 2.0 Flash Pen thumb Drive SDCZ50)");
		driver.findElement(By.xpath("//*[@id=\"gh-btn\"]")).click();
	}
	@And("^click the concerned product$")
	public void clickproduct() {
		driver.findElement(By.xpath("//a[@class='s-item__link']/h3[contains(text(),'SanDisk 8GB 16GB 32GB 64GB Cruzer Blade')]")).click();
	}
	@And("^select storage capacity$")
	public void selectstorage() {
		Select selectobject = new Select(driver.findElement(By.xpath("//select[@name='Storage Capacity']")));
		selectobject.selectByValue("2");
	}
	@And("^click add to cart button$")
	public void selectstorage2() {
		driver.findElement(By.xpath("//a[@id='isCartBtn_btn']")).click();
	}
	@Then("^make quantity two and verify the product's price has been doubled$")
	public void doublequantity() throws InterruptedException {
	String text =	driver.findElement(By.xpath("//div[@class='item-price']//span")).getText();
	text = text.substring(4,8);
	System.out.println(text);
	float value = Float.valueOf(text);
	System.out.println(value);
	Select selectobject = new Select(driver.findElement(By.xpath("//select[@data-test-id='qty-dropdown']")));
	selectobject.selectByValue("2");
	Thread.sleep(2000);
	String text2 =	driver.findElement(By.xpath("//div[@class='item-price']//span")).getText();
	text2 = text2.substring(4,8);
	float value2 = Float.valueOf(text2);
	System.out.println(value2);
	if((value*2)==value2) {
		System.out.println("It has been doubled");
	}
	}

}
