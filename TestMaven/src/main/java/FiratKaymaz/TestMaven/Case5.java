package FiratKaymaz.TestMaven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Case5 {
	
public static WebDriver driver;
	
	@Given("^visit to ebay website$")	
	public void accesstoEbay() {
		WebDriverManager.firefoxdriver().setup();
		//System.setProperty("webdriver.gecko.driver","/Users/firatkaymaz/eclipse-workspace/SeleniumTest/drivers/geckodriver/geckodriver");
		driver = new FirefoxDriver();
		driver.get("https://www.ebay.com/");		
	}
	@And("^click motors tab$")
	public void motortab() {
		driver.findElement(By.xpath("/html/body/div[4]/div[1]/ul/li[6]/a")).click();
	}
	@And("^click cars and trucks on the left side menu$")
	public void carsandtrucks() {
		driver.findElement(By.xpath("//span[contains(@class, 'title') and text() = 'Cars & Trucks']")).click();
	}
	@And("^select a random brand$")
	public void selectabrand() {
		int i = getRandomInteger(1,20);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/ul/li[1]/ul/li[1]/ul/li["+i+"]/a/span")).click();
	}
	@And("^click the item$")
	public void selecttheitem() throws InterruptedException {
		int i = getRandomInteger(1,10);
		Thread.sleep(1000);
		driver.findElement(By.xpath("/html/body/div[3]/div[4]/div[2]/div[1]/div[2]/ul/li["+i+"]/div/div[1]/div/a/div/img")).click();
		String  brand = driver.findElement(By.xpath("//span[@class='g-hdn']")).getText();
	     System.out.println(brand);
	}
	
	 public static int getRandomInteger(int maximum, int minimum){
	        return ((int) (Math.random()*(maximum - minimum))) + minimum;
	    }
	 public static String brandname()
	   {  
	   String  brand = driver.findElement(By.xpath("//span[@class='g-hdn']")).getText();
	     System.out.println(brand);
	      return brand;
	   }
	 @When("^go to recently viewed through my ebay$")
	 public void login() throws InterruptedException {
		    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/header/div/ul[2]/li[1]/div/a[1]")).click();
			driver.findElement(By.xpath("//*[@id=\"userid\"]")).sendKeys("firat_14");
			driver.findElement(By.xpath("//*[@id=\"pass\"]")).sendKeys("1a2b3c4d");
			driver.findElement(By.xpath("//*[@id=\"sgnBt\"]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[2]/div[2]/p[2]/a")).click();	
		}
	 @Then("^verify the item viewed in latest is in the recently viewed screen$")
	 public void VerifyRecentlyViewed() throws InterruptedException {
		Thread.sleep(2000);                     
		String text =	driver.findElement(By.xpath("/html/body/div[3]/div[2]/div[3]/div/div/div[3]/div[2]/ul/li[1]/div/div[2]/a")).getText();
		if(text.contains(brandname())) {
			System.out.println("item viewed as latest is in the recently viewed screen");
		}
		}

}
